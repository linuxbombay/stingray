// Server manager
function attemptLoad() {
    var urlsToCheck = [
        'http://100.93.238.99:8096' // VPN Server
        //'http://exampledomain1:8096', // Domain 1
        //'http://exampledomain2:8096' // Domain 2
    ];
    var fallbackPage = 'appoffline.html';
    var attemptsMade = 0;

    function attemptSingleUrl(url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.timeout = 20000; // Timeout after 20 seconds
        xhr.ontimeout = function () {
            // Redirect to fail.html if the request times out
            window.location.href = fallbackPage;
        };
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status >= 200 && xhr.status < 300) {
                    window.location.href = xhr.responseURL || url;
                } else {
                    attemptsMade++;
                    if (attemptsMade < urlsToCheck.length) {
                        attemptSingleUrl(urlsToCheck[attemptsMade]);
                    } else {
                        window.location.href = fallbackPage;
                    }
                }
            } else {
                console.error("Request state changed but not completed yet.");
            }
        };
        xhr.onerror = function() {
            console.error("Network error occurred.");
        };
        xhr.send();
    }

    setTimeout(function() {
        attemptSingleUrl(urlsToCheck[0]);
    }, 3000); // Delay before starting the process
}

window.onload = attemptLoad;

