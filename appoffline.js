document.addEventListener('DOMContentLoaded', function() {
    const statusPill = document.getElementById('status-pill');
    const statusText = document.getElementById('status-text'); // New reference for status text
    const spinner = document.getElementById('spinner');

    // Set initial status to red and offline
    statusPill.classList.add('red');
    statusText.textContent = "Offline"; // Update the span text directly

    function checkStatus() {
        // Change to yellow and show checking status
        statusPill.classList.remove('red', 'green');
        statusPill.classList.add('yellow');
        statusText.textContent = "Checking status..."; // Update the span text
        spinner.style.display = 'inline-block'; // Show spinner

        fetch('http://100.93.238.99:7000/status.txt')
            .then(response => {
                // Hide spinner
                spinner.style.display = 'none';

                if (response.ok) {
                    statusPill.classList.remove('yellow');
                    statusPill.classList.add('green');
                    statusText.textContent = "Online, reconnecting.."; // Update the span text

                    // Set a timeout to redirect after 3 seconds (3000 milliseconds)
                    setTimeout(function() {
                        window.location.href = "./splash.html"; // Replace with your local file path
                    }, 3000);
                } else {
                    statusPill.classList.remove('green');
                    statusPill.classList.add('red');
                    statusText.textContent = "Offline"; // Update the span text
                }
            })
            .catch(() => {
                // Hide spinner
                spinner.style.display = 'none';
                statusPill.classList.remove('green');
                statusPill.classList.add('red');
                statusText.textContent = "Offline"; // Update the span text
            });
    }

    checkStatus(); // Initial check
    setInterval(checkStatus, 30 * 1000); // Check every 15 seconds
});

