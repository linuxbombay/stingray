// Modules to control application life and create native browser window
const { app, BrowserWindow, shell } = require('electron');
const path = require('path');
const fs = require('fs');
const https = require('https');

// Dynamically import electron-context-menu (default export)
import('electron-context-menu').then((contextMenuModule) => {
  const contextMenu = contextMenuModule.default;

  contextMenu({
    prepend: (defaultActions, parameters, browserWindow) => [
      //{
        //label: 'Save Image',
        // Only show it when right-clicking images
        //visible: parameters.mediaType === 'image'
      //},
      {
        label: 'Search Google for “{selection}”',
        // Only show it when right-clicking text
        visible: parameters.selectionText.trim().length > 0,
        click: () => {
          shell.openExternal(`https://google.com/search?q=${encodeURIComponent(parameters.selectionText)}`);
        }
      }
    ]
  });
});

let mainWindow;

async function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1360,
    height: 765,
    icon: path.join(__dirname, 'icon.png'),
    backgroundColor: '#2C2C2C',
    webPreferences: {
      contextIsolation: true,
      spellcheck: true,
      nodeIntegration: false
    }
  });

  // Load the splash page first, then redirect to the store page
  setTimeout(() => {
     mainWindow.loadFile('splash.html');
  }, 3000); // Load store page after 3 secs

  mainWindow.maximize(); // Start maximized
  mainWindow.setMenuBarVisibility(false);
  mainWindow.setMenu(null);
  mainWindow.show();

  // Open external links in the default browser
  mainWindow.webContents.setWindowOpenHandler(({ url }) => {
    shell.openExternal(url);
    return { action: 'deny' }; // Prevent opening the link in the current window
  });

  // Open DevTools (optional)
  // mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished initialization
app.whenReady().then(createWindow);

// Quit the app when all windows are closed
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit();
});

// Re-create the window if the app is activated on macOS (when no windows are open)
app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

